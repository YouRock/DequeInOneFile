#pragma once//iterators not in deque
//#include <bits/stdc++.h>

#include <memory>
#include <memory.h>
#include <iostream>
#include <iterator>

template<typename T>
class Deque;

template<typename T>
using deq_ptr = std::shared_ptr<Deque<T> >;

template<typename T, class TDerived>
class base_iterator : public std::iterator<std::random_access_iterator_tag, T> {
public:
    base_iterator() : ptr(nullptr), _shift(0), _sizeOfBuffer(0) {}
    base_iterator(const std::shared_ptr<T>& p, const long long shift, const size_t& sizeOfBuf, const std::shared_ptr<Deque<T> >& pDeq) : 
		ptr(p), _shift(shift), _sizeOfBuffer(sizeOfBuf), _deq(pDeq) {}

    base_iterator(const base_iterator& it) : ptr(it.ptr), _shift(it._shift), _sizeOfBuffer(it._sizeOfBuffer), _deq(it._deq) {}

    TDerived operator+(long long shift) const {
        /*return *(base_iterator::ptr.get() + (base_iterator::_shift + base_iterator::_sizeOfBuffer
                                             - base_iterator::shift) % base_iterator::_sizeOfBuffer);*/

        TDerived tmp(static_cast<TDerived>(*this));
        //TDerived tmp(*this);
        tmp.AddToShiftWithout(shift);//CHECK!!!

        return tmp;
    }

    TDerived operator-(long long shift) const {
        return operator+(-shift);
    }

    int operator-(const TDerived sec) const { //CHECK!!!!!!!!!!
        int sz = _sizeOfBuffer;

/*		TDerived testing = sec;
		if (_shift - testing._shift == 0 && deq != 0) {
			testing--;
			if (testing._shift > _shift || testing._shift == _sizeOfBuffer - 1)
				return _recSize;
			else
				return -_recSize;
		}


		if (abs(_shift + (_sizeOfBuffer - sec._shift)) == _recSize)
			return _shift + (_sizeOfBuffer - sec._shift);
		else
			return _shift - sec._shift;*/
    }

	int operator+(const TDerived sec) const { //CHECK!!!!!!!!!!
		int sz = _sizeOfBuffer;
		return (_shift + sec.shift()) % sz;
	}

    TDerived& operator +=(size_t shift){
        AddToShift(shift);
        return static_cast<TDerived&>(*this);
    }

    TDerived& operator -=(size_t shift){
        AddToShift(-shift);
        return static_cast<TDerived&>(*this);
    }

    TDerived& operator--() {
		
        base_iterator::_shift--;
        base_iterator::_shift = (base_iterator::_shift + base_iterator::_sizeOfBuffer) % base_iterator::_sizeOfBuffer;

        return static_cast<TDerived&>(*this);
    }

    TDerived operator--(int) {
        base_iterator tmp(*this);
		
        base_iterator::_shift--;
        base_iterator::_shift = (base_iterator::_shift + base_iterator::_sizeOfBuffer) % base_iterator::_sizeOfBuffer;
        return tmp;
    }

    TDerived& operator++() {
        //T* tmp = ptr.get();
		

        ++base_iterator::_shift;
        base_iterator::_shift = (base_iterator::_shift + base_iterator::_sizeOfBuffer) % base_iterator::_sizeOfBuffer;


        return static_cast<TDerived&>(*this);
    }

    TDerived operator++(int) {
        base_iterator tmp(*this);
        ++base_iterator::_shift;
		
        base_iterator::_shift = (base_iterator::_shift + base_iterator::_sizeOfBuffer) % base_iterator::_sizeOfBuffer;
        return tmp;
    }

    void move(const std::shared_ptr<T>& p, size_t sizeOfBuf) {
        ptr = p;
        _sizeOfBuffer = sizeOfBuf;
    }

    void AddToShift(long long shift) {
        _shift += shift;
        _shift %= _sizeOfBuffer;
    }

    long long shift() const {
        return _shift;
    }

    void AddToShiftWithout(long long arg_shift){
        _shift += arg_shift;
    }

	void UpdateDeque(std::shared_ptr<Deque<T> > p) {
		//_deq = p;
	}

protected:

    std::shared_ptr<T> ptr; //you need several pointers
    long long _shift;
    size_t _sizeOfBuffer;
	std::shared_ptr<Deque<T> > _deq;
};

template <typename T>
class iterator : public base_iterator<T, class iterator<T> >{
    using base = base_iterator<T, class iterator<T> >;

public:


    iterator() = default;
    iterator(std::shared_ptr<T> p, size_t sizeOfBuf, std::shared_ptr<Deque<T> > pDeq) : base(p, 0, sizeOfBuf, pDeq) {}
    iterator(const base& bi) : base(bi) {}
    iterator(const iterator& it) = default;

    operator base() {
        return base(base::ptr,
                    base::_shift, base::_sizeOfBuffer);
    }

    T& operator*() {
        return *(base::ptr.get() + (base::_shift + base::_sizeOfBuffer)
                                                      % base::_sizeOfBuffer);
    }

    T* operator->() {
        return (base::ptr.get() + (base::_shift + base::_sizeOfBuffer)
                                                     % base::_sizeOfBuffer);
    }

    bool operator==(const iterator& right) const {
        return base::ptr == right.ptr &&
               base::_shift == right._shift &&
               base::_sizeOfBuffer == right._sizeOfBuffer;
    }

    bool operator!=(const iterator& right) const {
        return !operator==(right);
    }

	bool operator<(const iterator& right) const {
		return base::_shift < right._shift;
	}

	bool operator<=(const iterator& right) const {
		return base::_shift <= right._shift;
	}

    long long cshift() const {
        return base::_shift;
    }

    size_t sizeOfBuffer() const {
        return base::_sizeOfBuffer;
    }

    const std::shared_ptr<T> getPtr() const {
        return base::ptr;
    }
};

template <typename T>
class const_iterator : public base_iterator<T, const_iterator<T> > {
    using base = base_iterator<T , const_iterator<T> >;

public:
    const_iterator() = default;
    const_iterator(std::shared_ptr<T> p, size_t sizeOfBuf) : base(p, 0, sizeOfBuf) {}
    const_iterator(const base& bi) : base(bi) {}
    const_iterator(const const_iterator& it) = default;
    const_iterator(const std::shared_ptr<T> p, const long long shift, const size_t sizeOfBuf, const std::shared_ptr<Deque<T> > pDeq) :
            base(p, shift, sizeOfBuf, pDeq) {}


    operator base() {
        return base(base::ptr,
                                             base::_shift, base::_sizeOfBuffer);
    }

    T& operator*() const { //2:03 10.12 CHECK!!!
        return *(base::ptr.get() + (base::_shift + base::_sizeOfBuffer)
                                                            % base::_sizeOfBuffer);
    }

    const T* operator->() {
        return (base::ptr.get() + (base::_shift + base::_sizeOfBuffer)
                                                     % base::_sizeOfBuffer);
    }

    bool operator==(const const_iterator& right) const {
        return base::ptr == right.ptr &&
               base::_shift == right._shift &&
               base::_sizeOfBuffer == right._sizeOfBuffer;
    }

    bool operator!=(const const_iterator& right) const {
        return !operator==(right);
    }

	bool operator<(const const_iterator& right) const {
		return base::_shift < right._shift;
	}

	bool operator<=(const const_iterator& right) const {
		return base::_shift <= right._shift;
	}

    long long cshift() const {
        return base::_shift;
    }

    size_t sizeOfBuffer() const {
        return base::_sizeOfBuffer;
    }

    const std::shared_ptr<T> getPtr() const {
        return base::ptr;
    }
};


template<typename T>
class Deque {
public:
    Deque(); //finish it
    Deque(Deque&);
    ~Deque() = default;

    void push_back(const T& elem);
    void pop_back();
    void push_front(const T& elem);
    void pop_front();

    const T back() const; //need to check. Are you sure that here is T, but not T&
    const T front() const;//need to check. Are you sure that here is T, but not T&
    T& back();
    T& front();

    T& operator[] (size_t index);
    const T operator[] (size_t index) const; //need to check. Are you sure that here is T, but not T&

    bool empty() const;
    size_t size() const;

    /*template <class TDerived>
    class base_iterator;*/

    //class iterator;

    typedef ::iterator<T> iterator;
    typedef ::const_iterator<T> const_iterator;

    typedef std::reverse_iterator<iterator > reverse_iterator;

    //class const_iterator;
    typedef std::reverse_iterator<const_iterator > const_reverse_iterator;

    iterator begin();
    const_iterator begin() const;
    const_iterator cbegin() const;

    iterator end();
    const_iterator end() const;
    const_iterator cend() const;

    reverse_iterator rbegin();
    const_reverse_iterator rbegin() const;
    const_reverse_iterator crbegin() const;

    reverse_iterator rend();
    const_reverse_iterator rend() const;
    const_reverse_iterator crend() const;

private:
    void _ReduceCapacity();
    void _IncreaseCapacity();
    void _MoveToNewMemory();
	void UpdateIterators();

    size_t recSize=0;
    size_t sizeOfBuffer;
    size_t defaultSizeOfBuffer = 4;

    iterator begin_iter;
    iterator end_iter;

    std::shared_ptr<T> arr;
};

template <typename T>
Deque<T>::Deque() {
    sizeOfBuffer = defaultSizeOfBuffer;
    arr = std::shared_ptr<T>(new T[sizeOfBuffer]);

    begin_iter = iterator(arr, sizeOfBuffer, std::shared_ptr<Deque<T> >(this));
    end_iter = iterator(arr, sizeOfBuffer, std::shared_ptr<Deque<T> >(this)); //CHECK PLEASE
}

template <typename T>
Deque<T>::Deque(Deque<T>& deq) {
    recSize = deq.size();

    arr = std::shared_ptr<T>(new T[recSize]);
    *arr = *deq.arr;
}

/*template <typename T>
Deque<T>::~Deque() {

}*/

template <typename T>
void Deque<T>::push_back(const T& elem) { //probably works
    if ((recSize + 1) % sizeOfBuffer == 1 && (recSize + 1) / sizeOfBuffer >= 1){
        sizeOfBuffer *= 2;
        _IncreaseCapacity();
    }

    *end_iter = elem;

    end_iter++;
    recSize++;

	UpdateIterators();
}

template <typename T>
void Deque<T>::pop_back() { //probably works
    if (!empty()) {
        recSize--;
        end_iter--;
		
    }

    if (recSize == sizeOfBuffer / 4 && sizeOfBuffer != defaultSizeOfBuffer) {
        sizeOfBuffer /= 2;
        _ReduceCapacity();
    }

	UpdateIterators();
}

template <typename T>
void Deque<T>::push_front(const T& elem) {
    if ((recSize + 1) % sizeOfBuffer == 1 && (recSize + 1) / sizeOfBuffer >= 1) {
        sizeOfBuffer *= 2;
        _IncreaseCapacity();
    }

    begin_iter--;
    *begin_iter = elem;

    recSize++;

	UpdateIterators();
}

template <typename T>
void Deque<T>::pop_front() {
    if (!empty()) {
        recSize--;
        begin_iter++;
		
    }

    if (recSize == sizeOfBuffer / 4 && sizeOfBuffer != defaultSizeOfBuffer) {
        sizeOfBuffer /= 2;
        _ReduceCapacity();
    }

	UpdateIterators();
}

template <typename T>
T& Deque<T>::back() {
    return operator[](recSize-1);
}

template <typename T>
const T Deque<T>::back() const {
    return operator[](recSize - 1);
}

template <typename T>
T& Deque<T>::front() {
    return operator[](0);
}

template <typename T>
const T Deque<T>::front() const {
    return operator[](0);
}

template <typename T>
T& Deque<T>::operator[] (size_t index) { //probably works
    size_t shift = (index + begin_iter.shift() + sizeOfBuffer/*+ 1*/) % sizeOfBuffer;
    return arr.get()[shift];
}

template <typename T>
const T Deque<T>::operator[] (size_t index) const {
    size_t shift = (index + begin_iter.shift() + sizeOfBuffer/*+ 1*/) % sizeOfBuffer;//size_t shift = (index - begin_iter.shift()) % recSize;
    return arr.get()[shift];
}

template <typename T>
bool Deque<T>::empty() const {
    return recSize == 0;
}

template <typename T>
size_t Deque<T>::size() const {
    return recSize;
}

template <typename T>
iterator<T> Deque<T>::begin() {
    return begin_iter;
}

template <typename T>
const_iterator<T> Deque<T>::begin() const { //везде убрать ссылки!!!!
    const_iterator ci(begin_iter.getPtr(), begin_iter.cshift(),
                                begin_iter.sizeOfBuffer());
    return ci;
}

template <typename T>
iterator<T> Deque<T>::end() {
    return end_iter;
}



template <typename T>
const_iterator<T> Deque<T>::end() const { //везде убрать ссылки!!!!???
    const_iterator ci(end_iter.getPtr(), end_iter.cshift(),
                                end_iter.sizeOfBuffer());
    return ci;
}

template <typename T>
typename Deque<T>::reverse_iterator Deque<T>::rend() {
    typename Deque<T>::reverse_iterator ri(begin_iter);
    return ri;
}

template <typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rend() const { //12/12/2017
    typename Deque<T>::const_reverse_iterator ri(cbegin());
    return ri;
}

template <typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crend() const {
    typename Deque<T>::const_reverse_iterator ri(cbegin());
    return ri;
}

template <typename T>
typename Deque<T>::reverse_iterator Deque<T>::rbegin() {
    typename Deque<T>::reverse_iterator ri(end_iter);//12/12/17
    return ri;
}

template <typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::rbegin() const {
    /*Deque<T>::const_iterator ci(end_iter.getPtr(), end_iter.cshift(),
                                end_iter.sizeOfBuffer());*/

    Deque<T>::const_reverse_iterator cri(cend());
    return cri;
}

template <typename T>
typename Deque<T>::const_reverse_iterator Deque<T>::crbegin() const{ //CHECK!!!

    /*Deque<T>::const_iterator ci(end_iter.getPtr(), end_iter.cshift(),
                                         end_iter.sizeOfBuffer());*/

    //Deque<T>::const_reverse_iterator cri(ci);

    return Deque<T>::const_reverse_iterator(cend());
}

template <typename T>
const_iterator<T> Deque<T>::cbegin() const {
    const_iterator ci(begin_iter.getPtr(), begin_iter.cshift(),
                                begin_iter.sizeOfBuffer(), deq_ptr<T>(this));
    return ci;
}



template <typename T>
const_iterator<T> Deque<T>::cend() const {
    const_iterator ci(end_iter.getPtr(), end_iter.cshift(),
                                end_iter.sizeOfBuffer(), deq_ptr<T>(this));
    return ci;
}



template <typename T>
void Deque<T>::_IncreaseCapacity() {
    //_MoveToNewMemory();
    std::shared_ptr<T> temp(new T[sizeOfBuffer]);

    if (end_iter.shift() <= begin_iter.shift()) {
        memcpy(temp.get(), arr.get(), sizeof(T) * end_iter.shift());

        memcpy(temp.get() + sizeOfBuffer / 2 + begin_iter.shift(), arr.get() + begin_iter.shift(),
               sizeof(T)*(recSize - begin_iter.shift()));

        begin_iter.move(temp, sizeOfBuffer);
        begin_iter.AddToShift (sizeOfBuffer / 2);

        end_iter.move(temp, sizeOfBuffer);

    } else {
        memcpy(temp.get(), arr.get() + begin_iter.shift(),
               sizeof(T)*(end_iter.shift() - begin_iter.shift()));

        begin_iter.move(temp, sizeOfBuffer);
        end_iter.move(temp, sizeOfBuffer);
    }

    arr = temp;


}


template <typename T>
void Deque<T>::_ReduceCapacity() {
    //_MoveToNewMemory();
    std::shared_ptr<T> temp(new T[sizeOfBuffer]);

    if (end_iter.shift() <= begin_iter.shift()) {
        memcpy(temp.get(), arr.get(), sizeof(T) * end_iter.shift());

        memcpy(temp.get() - sizeOfBuffer + begin_iter.shift(), arr.get() + begin_iter.shift(),
               sizeof(T)*(sizeOfBuffer * 2 - begin_iter.shift()));

        begin_iter.move(temp, sizeOfBuffer);
        begin_iter.AddToShift(-sizeOfBuffer);

        end_iter.move(temp, sizeOfBuffer);

    } else {
        memcpy(temp.get(), arr.get() + begin_iter.shift(),
               sizeof(T)*(end_iter.shift() - begin_iter.shift()));

        begin_iter = iterator(temp, sizeOfBuffer, deq_ptr<T>(this));
        end_iter = iterator(temp, sizeOfBuffer, deq_ptr<T>(this));
        end_iter.AddToShift(recSize);

        end_iter.move(temp, sizeOfBuffer);
    }

    arr = temp;


}

template <typename T>
void Deque<T>::_MoveToNewMemory() {
    std::shared_ptr<T> temp(new T[sizeOfBuffer]);

    //long long endShift = (end_iter.shift() == 0) ? 1 : end_iter.shift();

    size_t begin = 0;
    if (begin_iter.shift() < end_iter.shift())
        begin = begin_iter.shift();

    if(begin_iter.shift() > end_iter.shift())
        memcpy(temp.get(), arr.get() + begin_iter.shift(), sizeof(T) * (end_iter.shift() - begin)); //CHECK!!!! IT

    //long long endShift = (end_iter.shift() == 0) ? 1 : end_iter.shift();

/*	size_t shift = sizeOfBuffer + begin_iter.shift(); //maybe +1
size_t countOfLastElem = (abs(long long(begin_iter.shift())));
memcpy(temp.get() + shift, arr.get() + recSize + begin_iter.shift(), sizeof(T) * countOfLastElem);*/

    if (begin_iter.shift() < end_iter.shift()) {
        size_t shift = sizeOfBuffer / 2 + begin_iter.shift(); //maybe +1
    }
    size_t shift = sizeOfBuffer/2 + begin_iter.shift(); //maybe +1
    //���� ������ ������� ������� ���������� � ����������
    size_t countOfLastElem = (sizeOfBuffer / 2 - end_iter.shift());
    memcpy(temp.get() + shift, arr.get() + begin_iter.shift(), sizeof(T) * countOfLastElem);

    arr = temp;
}

template <typename T>
void Deque<T>::UpdateIterators() {
	begin_iter.UpdateDeque(deq_ptr<T>(this));
	end_iter.UpdateDeque(deq_ptr<T>(this));
}

/*template <typename T>
class Deque<T>::const_iterator : public base { //: public iterator {
    using base = base;
    //friend Deque<T>::iterator;

public:
    const_iterator() : base() {}
    const_iterator(const std::shared_ptr<T> p, const long long shift, const size_t sizeOfBuf) :
            base(p, shift, sizeOfBuf) {}


    const_iterator(base& bi) : base(bi) {}

    const_iterator(const_iterator& it) :
            base(it) {}


    long long cshift() const {
        return base::_shift;
    }

    size_t sizeOfBuffer() const {
        return base::_sizeOfBuffer;
    }

    const std::shared_ptr<T> getPtr() const { //maybe delete const
        return base::ptr;
    }

    bool operator==(const const_iterator& right) const {
        return base::ptr == right.ptr
               && base::_shift == right._shift
               && base::_sizeOfBuffer == right._sizeOfBuffer;
    }

    bool operator!=(const const_iterator& right) const {
        return !operator==(right);
    }

    const T& operator*()  const {
        return *base::ptr;
    }

    const_iterator& operator=(const_iterator ci) {
        base::ptr = ci.getPtr();
        base::_shift = ci.cshift();
        base::_sizeOfBuffer = ci.sizeOfBuffer();

        return (*this);
    }


//const_iterator(base& bi) : base(bi) {}

const_iterator(const typename  Deque<T>::iterator& it) :
base::ptr(it.ptr) {}//base(static_cast<base >(it)) {}


};*/
