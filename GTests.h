﻿#pragma once

#include <ctime>
#include <deque>
#include <string>
#include <vector>

#include "deque.h"
#include <gtest/gtest.h>
#include <gmock/gmock.h>

template <typename T>
::testing::AssertionResult EqualElements (std::string whence, T right, T left){
    if(right != left){
        return testing::AssertionFailure() <<"Test from " << whence.c_str() <<  " failed. "
                 << right << " != " << left;
    } else
        return testing::AssertionSuccess();

}

template <typename T>
void EqualDeques(std::string whence, std::deque<T>& trueDeque, Deque<T>& myDeque) {
	for (size_t i = 0; i < trueDeque.size(); i++) {
		ASSERT_TRUE(EqualElements(whence, trueDeque[i], myDeque[i]));
	}
}

template<typename T>
void UpdateDeques(size_t size, std::deque<T>& trueDeq, Deque<T>& myDeq) {
	trueDeq.clear();
	myDeq = Deque<T>();

	FillDeques(size, trueDeq, myDeq);
}

template<typename T>
void FillDeques(size_t size, std::deque<T>& trueDeq, Deque<T>& myDeq) {
	for (size_t i = 0; i < size; i++) {
		T var = rand() % 256;
		trueDeq.push_back(var);
		myDeq.push_back(var);
	}
}



TEST(DequeTest, SimpleAndCopyConstructor) {
	std::deque<int> trueDeq;
	std::deque<int> trueDeq2;
	Deque<int> myDeq;
	Deque<int> myDeq2;

	FillDeques(rand() % 100000, trueDeq, myDeq);
	
	trueDeq2 = trueDeq;
	myDeq2 = myDeq;

	EqualDeques("First appropriation", trueDeq2, myDeq2);

	FillDeques(9999, trueDeq2, myDeq2);
	trueDeq = trueDeq2;
	myDeq = myDeq2;

	EqualDeques("Second appropriation", trueDeq, myDeq);
}

TEST(DequeTest, EmptyAndSize) {
	std::deque<int> trueDeque;
	Deque<int> myDeque;

	for (size_t i = 0; i < 1000; i++) {
		ASSERT_EQ(trueDeque.empty(), myDeque.empty());
		ASSERT_EQ(trueDeque.size(), myDeque.size());

		FillDeques(rand() % 1000, trueDeque, myDeque);
	}
}

template <typename T>
void EqualBackAndFront(std::deque<T>& trueDeque, Deque<T>& myDeque) {
	const T ctrueElem = trueDeque.front();
	T trueElem = const_cast<T&>(ctrueElem);

	const T cmyElem = myDeque.front();
	T myElem = const_cast<T&>(cmyElem);

	ASSERT_TRUE(EqualElements("const front()", trueElem, myElem));

	const T ctrueElem2 = trueDeque.back();
	trueElem = const_cast<T&>(ctrueElem2);

	const T cmyElem2 = myDeque.back();
	myElem = const_cast<T&>(cmyElem2);

	ASSERT_TRUE(EqualElements("const back()", trueElem, myElem));
}

TEST(DequeTest, PushPopSquareBracketsBackAndFront) {
    //srand(time(NULL));

	typedef int type;

    std::deque<type> trueDeque;
	Deque<type> myDeque;

    for (size_t i = 0; i < 10000; i++) {
		ASSERT_EQ(myDeque.size(), trueDeque.size());

    	int decision = rand() % 5;

    	switch (decision) {
    	case 0:
    	{
            if (!myDeque.empty() && !trueDeque.empty()) {
				int index = rand() % trueDeque.size();
                ASSERT_TRUE(EqualElements("operator []", trueDeque[index], myDeque[index]));

				const type ctrueElem = trueDeque[index];
				type trueElem = const_cast<int&>(ctrueElem);

				const type cmyElem = trueDeque[index];
				type myElem = const_cast<int&>(ctrueElem);

				ASSERT_TRUE(EqualElements("const operator []", trueElem, myElem));

				ASSERT_TRUE(EqualElements("back()", trueDeque.back(), myDeque.back()));
				ASSERT_TRUE(EqualElements("front()", trueDeque.front(), myDeque.front()));

				EqualBackAndFront(trueDeque, myDeque);
            }

			break;
    	}

		case 1:
		{
			if (!myDeque.empty() && !trueDeque.empty()) {
                testing::AssertionSuccess()<<"pop_front(";
				myDeque.pop_front();
				trueDeque.pop_front();
				break;
			}
		}

		case 2:
		{
			if (!myDeque.empty() && !trueDeque.empty()) {
				myDeque.pop_back();
				trueDeque.pop_back();
				break;
			}
		}

		case 3:
		{
            int number = rand() % 100;
            myDeque.push_front(number);
            trueDeque.push_front(number);
            break;
		}

		case 4:
		{
            int number = rand() % 100;
			myDeque.push_back(number);
			trueDeque.push_back(number);

            //std::cout<<myDeque[myDeque.size() - 1] << " and " << trueDeque.size();
			break;
		}

    	default:
			//std::cout << "Your tests are like shit";
			break;
    	}
    }

	EqualDeques("Final compare", trueDeque, myDeque);
}

/*template <typename T, typename type_trueIt, typename type_myIt>
void TestIterators(size_t size, type_trueIt& trueIt, type_myIt& myIt, type_trueIt& trueBegin, type_trueIt& trueEnd) {
	for (size_t i = 0; i < size * 3; i++) {
		int decision = rand() % 3;

		switch (decision) {
			case 0:
			{
				ASSERT_TRUE(EqualElements("iterator::operator*", *trueIt, *myIt));
				break;
			}

			case 1:
			{
				if ((trueIt + 1) != trueEnd) {
					trueIt++;
					myIt++;
				}

				break;
			}

			case 2:
			{
				if (trueIt != trueBegin) {
					trueIt--;
					myIt--;
				}

				break;
			}

			default:
				break;
		}
	}
}*/

TEST(DequeTest, Iterators) {
	typedef int type;

	std::deque<type> trueDeque;
	Deque<type> myDeque;

	const size_t size = 5;
	FillDeques(size, trueDeque, myDeque);

	/*	std::deque<type>::iterator trueIt(trueDeque.begin());
	Deque<type>::iterator myIt;
	myIt = myDeque.begin();

	TestIterators<type, std::deque<type>::iterator, Deque<type>::iterator>
	(size * 3, trueIt, myIt, trueDeque.begin(), trueDeque.end());

	trueIt = trueDeque.end();
	myIt = myDeque.end();

	TestIterators<type, std::deque<type>::iterator, Deque<type>::iterator>
	(size * 3, trueIt, myIt, trueDeque.begin(), trueDeque.end());

	std::deque<type>::reverse_iterator rtrueIt = trueDeque.rbegin();
	Deque<type>::reverse_iterator rmyIt;
	rmyIt = myDeque.rbegin();

	TestIterators<type, std::deque<type>::reverse_iterator, Deque<type>::reverse_iterator>
	(size * 3, rtrueIt, rmyIt, trueDeque.rbegin(), trueDeque.rend());

	rtrueIt = trueDeque.rend();
	rmyIt = myDeque.rend();*/

	std::deque<type>::iterator trueItB(trueDeque.begin());
	Deque<type>::iterator myItB;
	myItB = myDeque.begin();


	std::deque<type>::iterator trueItE(trueDeque.end());
	Deque<type>::iterator myItE;
	myItE = myDeque.end();

	std::deque<type>::reverse_iterator trueItRB(trueDeque.rbegin());
	Deque<type>::reverse_iterator myItRB;
	myItRB = myDeque.rbegin();

	std::deque<type>::reverse_iterator trueItRE(trueDeque.rend());
	Deque<type>::reverse_iterator myItRE;
	myItRE = myDeque.rend();

	for (size_t i = 0; i < size * 3; i++) {
		int decision = rand() % 3;

		switch (decision) {
			case 0:
			{
				if(trueItB!=trueDeque.end())
					ASSERT_TRUE(EqualElements("operator*", *trueItB, *myItB));

				if (trueItE != trueDeque.end())
					ASSERT_TRUE(EqualElements("operator*", *trueItE, *myItE));

				if (trueItRB != trueDeque.rend())
					ASSERT_TRUE(EqualElements("operator*", *trueItRB, *myItRB));

				if (trueItRE != trueDeque.rend())
					ASSERT_TRUE(EqualElements("operator*", *trueItRE, *myItRE));

				break;
			}

			case 1:
			{
				if ((trueItB + 1) != trueDeque.end()) {
					trueItB++;
					myItB++;
				}

				if ((trueItRB + 1) != trueDeque.rbegin() && trueItRB != trueDeque.rbegin()) {
					trueItRB--;
					myItRB--;
				}

				if ((trueItE + 1) != trueDeque.end() && trueItE != trueDeque.end()) {
					trueItE++;
					myItE++;
				}

				if ((trueItRE + 1) != trueDeque.rbegin() && trueItRB != trueDeque.rbegin()) {
					trueItRE--;
					myItRE--;
				}

				break;
			}

			case 2:
			{
				if ((trueItB) != trueDeque.begin()) {
					trueItB--;
					myItB--;
				}

				if ((trueItRB) != trueDeque.rend()) {
					trueItRB++;
					myItRB++;
				}

				if ((trueItE) != trueDeque.begin()) {
					trueItE--;
					myItE--;
				}

				if ((trueItRE) != trueDeque.rend()) {
					trueItRE++;
					myItRE++;
				}

				break;
			}

			default:
				break;
		}
	}

}

